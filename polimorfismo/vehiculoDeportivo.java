/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polimorfismo;

/**
 *
 * @author Dell
 */
public class vehiculoDeportivo extends vehiculo{
    private int cilindrada;
    public vehiculoDeportivo(int cilindrada,String matricula, String marca, String modelo){
        super(matricula, marca, modelo);
        this.cilindrada=cilindrada;
    }

    public int getCilindrada() {
        return cilindrada;
    }
    @Override
    public String mostrarDatos(){
        return "Matricula: "+matricula+"\nMarca: "+marca+"\nModelo: "+modelo+
               "\nCilindrada: "+cilindrada;

    }
}
