/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polimorfismo;

/**
 *
 * @author Dell
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        vehiculo misVehiculos[]=new vehiculo[4];
        misVehiculos[0]=new vehiculo("LBB1254", "Chevrolet", "Camaro");
        misVehiculos[1]=new vehiculoTurismo(5, "HCM5214", "Toyota", "Fortuner");
        misVehiculos[2]=new vehiculoDeportivo(500, "LBA2541", "Audi", "R8");
        misVehiculos[3]=new VehiculoPesado(2000, "PBC8810", "Chevrolet", "NHR");
        
        for(vehiculo vehiculos:misVehiculos){
            System.out.println(vehiculos.mostrarDatos());
            System.out.println("");
        }
            
    }
    
}
