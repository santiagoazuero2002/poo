/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polimorfismo;

/**
 *
 * @author Dell
 */
public class VehiculoPesado extends vehiculo{
    private int carga;
    
    public VehiculoPesado(int carga ,String matricula, String marca, String modelo){
        super(matricula, marca, modelo);
        this.carga=carga;
        
    }

    public int getCarga() {
        return carga;
    }
    @Override
    public String mostrarDatos(){
        return "Matricula: "+matricula+"\nMarca: "+marca+"\nModelo: "+modelo+
                "\nCarga: "+carga;

    }
}
