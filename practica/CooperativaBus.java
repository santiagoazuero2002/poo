/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica;

/**
 *
 * @author Dell
 */
public class CooperativaBus {
    protected String nombreCoo;
    protected String fechaCreacionCoo;
    protected int numBusesCoo;
    
    public CooperativaBus(String nombreCoo, String fechaCreacionCoo, int numBusesCoo){
        this.nombreCoo=nombreCoo;
        this.fechaCreacionCoo=fechaCreacionCoo;
        this.numBusesCoo=numBusesCoo;
    }

    public String getNombreCoo() {
        return nombreCoo;
    }

    public String getFechaCreacionCoo() {
        return fechaCreacionCoo;
    }

    public int getNumBusesCoo() {
        return numBusesCoo;
    }
    public String mostrarDatos(){
        return "Cooperativa de buses (Padre)"+"\nNombre de Cooperativa: "+nombreCoo+"\nFecha de Creacion de COO: "+fechaCreacionCoo+
                "\nNumero de Buses de la COO: "+numBusesCoo;
    }
}
