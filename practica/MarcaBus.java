/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica;

/**
 *
 * @author Dell
 */
public class MarcaBus extends CooperativaBus{
    protected String Marca;
    protected String Modelo;
    public MarcaBus(String Marca, String Modelo, String nombreCoo, String fechaCreacionCoo, int numBusesCoo){
        super(nombreCoo, fechaCreacionCoo, numBusesCoo);
        this.Marca=Marca;
        this.Modelo=Modelo;
    }

    public String getMarca() {
        return Marca;
    }

    public String getModelo() {
        return Modelo;
    }
    @Override
    public String mostrarDatos(){
        return "Marca de buses(hijo)"+"\nNombre de Cooperativa: "+nombreCoo+"\nFecha de Creacion de COO: "+fechaCreacionCoo+
                "\nNumero de Buses de la COO: "+numBusesCoo+"\nMarca de bus: "+Marca+"\nModelo de bus: "+Modelo;
    }
}
