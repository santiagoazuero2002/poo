/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica;

/**
 *
 * @author Dell
 */
public class ChoferBus extends MarcaBus{
    protected String nombreChoferCOO;
    protected String edadChoferCOO;
    public ChoferBus(String nombreChoferCOO, String edadChoferCOO, String Marca, String Modelo, String nombreCoo, String fechaCreacionCoo, int numBusesCoo){
        super(Marca, Modelo, nombreCoo, fechaCreacionCoo, numBusesCoo);
        this.nombreChoferCOO=nombreChoferCOO;
        this.edadChoferCOO=edadChoferCOO;
    }

    public String getNombreChoferCOO() {
        return nombreChoferCOO;
    }

    public String getEdadChoferCOO() {
        return edadChoferCOO;
    }
    @Override
    public String mostrarDatos(){
        return "Nombre de Chofer (Nieto)"+"\nNombre de Cooperativa: "+nombreCoo+"\nFecha de Creacion de COO: "+fechaCreacionCoo+
                "\nNumero de Buses de la COO: "+numBusesCoo+"\nMarca de bus: "+Marca+"\nModelo de bus: "+Modelo+
                "\nNombre del chofer de cooperativa: "+nombreChoferCOO+"\nEdad de chofer de cooperativa: "+edadChoferCOO+" años";
    }
}
