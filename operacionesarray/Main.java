/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operacionesarray;

import java.util.Scanner;

/**
 *
 * @author Santiago
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner teclado=new Scanner(System.in);
        int matA[][]=new int[3][3];
        int matB[][]=new int[3][2];
        int matRes[][]=new int[3][3];            
        //if para verificar que se pueda realizar la multiplicacion
        //Escribir datos en la matrizA
        System.out.println("Datos de la Matriz A:");
        for(int i=0;i<matA.length;i++){
            for(int j=0;j<matA[i].length;j++){
                System.out.println("Digita un número para la posición: a["+i+"]["+j+"]"+" de la matriz A");
                matA[i][j]=teclado.nextInt();
//                System.out.print(matA[i][j]+" ");
            }
//            System.out.println();
        }
        //Escribir datos en la matrizB
        System.out.println("Datos de la Matriz B:");
        for(int i=0;i<matB.length;i++){
            for(int j=0;j<matB[i].length;j++){
                System.out.println("Digita un número para la posición: a["+i+"]["+j+"]"+" de la matriz B");
                matB[i][j]=teclado.nextInt();
            }
        }                      
        System.out.println("Resultado: ");      
        for ( int i = 0; i < matA.length; i++){ 
            for ( int j = 0; j < matB[i].length; j++){
                for ( int k = 0; k < matB.length; k++ ){ //puede ser columnasA o filasB ya que deben ser iguales
                    matRes[i][j] += matA[i][k]*matB[k][j];
                }
                System.out.print(matRes[i][j]+" ");
            }
            System.out.println();
        }
    }
}
