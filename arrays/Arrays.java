
package arrays;

import java.util.Scanner;

public class Arrays {
    public static void main(String[] args) {
        Scanner dato = new Scanner(System.in); 
        int edad[] = new int[5];
        for(int i=0;i<edad.length;i++){
           System.out.println("Ingrese su edad: ");
           edad[i] = dato.nextInt();      
        }  
        for(int i=0;i<edad.length;i++){
           System.out.println("El valor del vector en el indice"+"["+i+"]"+" es igual a; "+edad[i]);
        }
        if (edad[0]>edad[1] && edad[0]>edad[2] && edad[0]>edad[3]&& edad[0]>edad[4]) {
            System.out.println("La edad Mayor es: "+edad[0]);
        }else if (edad[1]>edad[0] && edad[1]>edad[2] && edad[1]>edad[3]&& edad[1]>edad[4]) {
            System.out.println("La edad mayor es: "+edad[1]);
        }else if (edad[2]>edad[0] && edad[2]>edad[1] && edad[2]>edad[3]&& edad[2]>edad[4]) {
            System.out.println("La edad mayor es: "+edad[2]);
        }else if (edad[3]>edad[0] && edad[3]>edad[1] && edad[3]>edad[2]&& edad[3]>edad[4]) {
            System.out.println("La edad mayor es: "+edad[3]);             
        }else{
            System.out.println("La edad mayor es: "+edad[4]);
        }         
    }        
}        