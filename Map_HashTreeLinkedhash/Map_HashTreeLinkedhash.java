/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map_hashtreelinkedhash;

import java.util.HashMap;
import java.util.TreeMap;
import java.util.LinkedHashMap;

/**
 *
 * @author Dell
 */
public class Map_HashTreeLinkedhash {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //HashMap
        HashMap<Integer, String> hashmap=new HashMap<>();
        hashmap.put(2, "Segundo");
        hashmap.put(1, "Primero");
        hashmap.put(5, "Quinto");
        hashmap.put(4, "Cuarto");
        hashmap.put(3, "Tercero");
        hashmap.put(2, "Dos");
        System.out.println(hashmap);
        
        
        TreeMap<String, Integer> treemap=new TreeMap<>();
        treemap.put("B", 2);
        treemap.put("G", 4);
        treemap.put("F", 2);
        treemap.put("P", 7);
        treemap.put("M", 6);
        treemap.put("G", 5);
        
        System.out.println(treemap);
        
        LinkedHashMap<Integer, String> linkedhaskmap=new LinkedHashMap<>();
        linkedhaskmap.put(2, "Batidor");
        linkedhaskmap.put(1, "Sacador");
        linkedhaskmap.put(3, "Punto centro delantero");
        linkedhaskmap.put(4, "Rematador izquierdo");
        linkedhaskmap.put(5, "Sacador izquierdo");
        linkedhaskmap.put(2, "Rematador");
        
        System.out.println(linkedhaskmap);
    }
    
}
