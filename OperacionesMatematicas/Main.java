/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OperacionesMatematicas;
import java.util.Scanner;
/**
 *
 * @author Santiago
 */
public class Main {
    public static void main(String[] args) {
        Scanner leer=new Scanner(System.in);
        int ejec=1;
        while (ejec==1) {            
            System.out.println("Ingrese la operacion a realizar: \n1.Suma \n2.Resta "
                    + "\n3.Multiplicación \n4.División");
            int a=leer.nextInt();
            switch (a){
                case 1:
                    System.out.print("Ingresa sumando 1: ");
                    int num1=leer.nextInt();
                    System.out.print("Ingresa sumando 2: ");
                    int num2=leer.nextInt();
                    System.out.print("Ingresa sumando 3: ");
                    int num3=leer.nextInt();
                    Suma suma=new Suma();
                    suma.Suma(num1, num2, num3);
                    double respuestaSuma=suma.imprimir();
                    System.out.println("La suma es: "+respuestaSuma);
                    break;
                case 2:
                    System.out.print("Ingresa minuendo: ");
                    int minuendo1=leer.nextInt();
                    System.out.print("Ingresa sustraendo: ");
                    int sustraendo1=leer.nextInt();
                    Resta resta=new Resta();
                    resta.Resta(minuendo1, sustraendo1);
                    double respuestaResta=resta.operacion();
                    System.out.println("La resta es: "+respuestaResta);
                    break;
                case 3:
                    System.out.println("Ingrese el primer valor de la multiplicacion: ");
                    double mult1=leer.nextDouble();
                    System.out.println("Ingrese el segundo valor de la multiplicacion: ");
                    double mult2=leer.nextDouble();
                    Multiplicacion multiplica=new Multiplicacion();
                    multiplica.Multiplicacion(mult1,mult2);
                    double respuesta2=multiplica.operacion();
                    System.out.println("La multiplicacion es: "+respuesta2);
                    break;
                case 4:
                    System.out.println("Ingrese el primer valor de la division: ");
                    double div1=leer.nextDouble();
                    System.out.println("Ingrese el segundo valor de la division: ");
                    double div2=leer.nextDouble();
                    Division divide=new Division();
                    divide.Division(div1, div2);
                    double respuesta3=divide.imprimir();
                    System.out.println("La division es: "+respuesta3);
                    break;
                default:
                    System.out.println("--No existe esta opcion--");
            }
            System.out.println("Desea seguir realizando las opercaiones? \nIngrese 1:Si o 2:No ");
            ejec=leer.nextInt();
        }
    }
}
