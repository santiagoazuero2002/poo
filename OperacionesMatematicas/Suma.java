/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OperacionesMatematicas;

/**
 *
 * @author user
 */
public class Suma {
    
    private double sumando1,sumando2,sumando3,resultado;
    public void Suma(double num1, double num2,double num3){
        sumando1=num1;
        sumando2=num2;
        sumando3=num3;
        resultado=num1+num2+num3;
    }
    public double operacion(){
        return sumando1+sumando2+sumando3;
    }
    public double imprimir(){
        return operacion();
    }

    public double getSumando1() {
        return sumando1;
    }

    public void setSumando1(double sumando1) {
        this.sumando1 = sumando1;
    }

    public double getSumando2() {
        return sumando2;
    }

    public void setSumando2(double sumando2) {
        this.sumando2 = sumando2;
    }

    public double getSumando3() {
        return sumando3;
    }

    public void setSumando3(double sumando3) {
        this.sumando3 = sumando3;
    }

    public double getResultado() {
        return resultado;
    }

    public void setResultado(double resultado) {
        this.resultado = resultado;
    }
    
}
