/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectofinal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Dell
 */
public class ConeccionBd {

    public String URL = "jdbc:mysql://localhost:3306/persona?characterEncoding=latin1";
    public String password = "root";
    public String user = "root";
    Scanner entradalet = new Scanner(System.in);
    Scanner numero = new Scanner(System.in);

    public Connection Coneccion() {
        Connection conexion = null;
        try {
            boolean t = true;
            do {
                System.out.println("Ingrese que desea hacer:\n0: Salir\n1: Ingresar Datos\n2: Mostrar tabla"
                        + "\n3: Buscar en la tabla utilizando nombre");
                switch (numero.nextInt()) {
                    case 0:
                        t = false;
                        System.out.println("Sesion Terminada.");
                        break;
                    case 1:
                        Class.forName("com.mysql.jdbc.Driver");
                        conexion = DriverManager.getConnection(URL, user, password);
                        System.out.println("Conexion establecida");
                        Statement sentencia = (Statement) conexion.createStatement();
                        System.out.println("Ingrese los datos de la persona");
                        System.out.print("Nombre: ");
                        String nombre = entradalet.next();
                        System.out.print("Apellido: ");
                        String apellido = entradalet.next();
                        System.out.print("Cédula: ");
                        String cedula = entradalet.next();
                        String sentenciaSql = "insert into persona values ('" + cedula + "','" + nombre + "','" + apellido + "')";
                        int insert = sentencia.executeUpdate(sentenciaSql);
                        sentencia.close();
                        conexion.close();
                        break;
                    case 2:
                        ArrayList<Persona> listaPersona;
                        Persona persona = null;
                        Class.forName("com.mysql.jdbc.Driver");
                        conexion = DriverManager.getConnection(URL, user, password);
                        System.out.println("Conexion establecida ");
                        Statement sentencia2 = (Statement) conexion.createStatement();
                        String sentenciaSql2 = "select * from persona";
                        ResultSet resultado = sentencia2.executeQuery(sentenciaSql2);
                        listaPersona = new ArrayList<Persona>();
                        while (resultado.next()) {
                            String id2 = resultado.getString("id");
                            String nombre2 = resultado.getString("nombre");
                            String apellido2 = resultado.getString("apellido");
                            persona = new Persona();
                            persona.setId(id2);
                            persona.setNombre(nombre2);
                            persona.setApellido(apellido2);
                            listaPersona.add(persona);
                        }
                        for (Persona j : listaPersona) {
                            System.out.println("" + j.imprimir());
                        }
                        sentencia2.close();
                        conexion.close();
                        break;
                    case 3:
                        ArrayList<Persona> listaPersona3;
                        Persona persona3 = null;
                        Class.forName("com.mysql.jdbc.Driver");
                        conexion = DriverManager.getConnection(URL, user, password);
                        System.out.println("Conexion establecida ");
                        Statement sentencia3 = (Statement) conexion.createStatement();
                        System.out.println("Ingrese el nombre a buscar");
                        String nombre3 = entradalet.next();
                        String sentenciaSql3 = "select * from persona where nombre='"+nombre3+"'";
                        ResultSet resultado3 = sentencia3.executeQuery(sentenciaSql3);
                        listaPersona3 = new ArrayList<Persona>();
                        while (resultado3.next()) {
                            String id3 = resultado3.getString("id");
                            String apellido3 = resultado3.getString("apellido");
                            persona3 = new Persona();
                            persona3.setId(id3);
                            persona3.setNombre(nombre3);
                            persona3.setApellido(apellido3);
                            listaPersona3.add(persona3);
                        }
                        for (Persona j : listaPersona3) {
                            System.out.println("" + j.imprimir());
                        }
                        sentencia3.close();
                        conexion.close();
                        break;

                }

            } while (t);

        } catch (Exception ex) {
            System.out.println("Error en la conexión:" + ex);
        }
        return conexion;
    }
}
